# Scylla

## Core classes

### App

An app manages a list of tasks and their configuration. When the `main` of an app is run without arguments or as a daemon, it will run through each task and sleep for a specified amount of time before running through the tasks again.

### Task

A task is a single process that the app can run. The Task class keeps track of when the process last ran successfully and what records failed. This is often extended directly for downloading records from an external api and then saving the results as Records in OrientDB for processing in other tasks.

#### UpdateTask

Runs a process on each record of a specific type, which was updated since the last time this ran successfully.

#### ReflectTask

This is an UpdateTask which gives a common starting point for reflecting data changes in one record type to another.
It will normally find changes to one record type, translate the data into a request using a Conversion, and then push that request into another system to Create or Update a record.
Afterwards it will save the result, and create an edge to log the change that took place.
The edge will also show how the 2 records are related, so that future changes can follow the same path without relying on external ids or lookups.

### Conversion

A conversion is a field mapping (dictionary of instructions), which will create a request dictionary from a Record that was loaded from OrientDB.


## OrientDB Schema

OrientDB is a multi-model database which allows for loosely-defined structured data like MongoDB, but also supports relational data, atomic operations, and graph traversals.
You can save and load full requests and responses for api endpoints, without flattening or serializing data.
It can also use edges to associate any record to any other record in an efficient way.

### Vertices

- Task: Tracks when a tasks was last completed.
- Error: Errors and exceptions while running a task.
  - RecordError: An error related to a record failure in a specific task.
    (These are automatically deleted once the operation is successful.)
- Record: A data object from an external system.
  Classnames begin with a prefix for the associated system.
  Fieldnames starting with an underscore are added by Scylla.

### Edges

- Reflection: The data in one system is represented by something in another system. (undirected)
  - Updated: One record was used to update an already existing record in another system.
  - Created: One record was used to create a record in another system.


## Scylla Modules

There generally 2 types of modules that are used in addition to the Scylla core to integrate systems.
- The first defines how to interact with a single application, including tasks that download records and abstract tasks to update them.
  Ex: [scylla-spree](https://gitlab.com/5stones/scylla-spree), [scylla-threepl](https://gitlab.com/5stones/scylla-threepl)
- Building on top of 2 of the above modules, another module will connect those two systems.
  This will contain the App, Tasks, and Conversions to keep data from the two systems in sync with each other.
  Ex: [scylla-spree-threepl](https://gitlab.com/5stones/scylla-spree-threepl)

__sections = {
    'OrientDB': {
        'host': '127.0.0.1:2480',
        'db': 'Scylla',
        'auth': ('admin', 'admin'),
        'is_ssl': False,
        'verify_ssl': True,
    },
    'Daemon': {
        #'name': None,
        'pid_dir': 'run',
        'log_dir': 'log',
    },
}


def getSection(name, defaults=None):
    if defaults:
        setDefaults(name, defaults)
    elif name not in __sections:
        __sections[name] = { 'name': name }
    return __sections[name]


def setDefaults(section_name, defaults):
    if section_name not in __sections:
        if 'name' not in defaults:
            defaults['name'] = section_name
        __sections[section_name] = defaults
    else:
        for (key, default) in defaults.items():
            setDefault(section_name, key, default)


def updateSection(name, properties):
    if name not in __sections:
        if 'name' not in properties:
            properties['name'] = name
        __sections[name] = properties
    else:
        __sections[name].update(properties)


def get(section_name, key, default=None):
    section = getSection(section_name)
    return section.get(key, default)


def setDefault(section_name, key, value):
    section = getSection(section_name)
    if key not in section:
        __sections[section_name][key] = value


def set(section_name, key, value):
    section = getSection(section_name)
    section[key] = value

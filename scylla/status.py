# -*- coding: utf-8 -*-
"""Tracks scylla status in a file which can be used for a health check

To track health of a process, set the environment variable SCYLLA_STATUS_FILE
and run `python -m scylla.status` for a health check.

You can also programmatically set scylla.status.FILENAME, and include the
filename and threshold, in seconds:
`python -m scylla.status 600 run/scylla-status.txt`.
"""
import sys
import time
import os


DEFAULT_THRESHOLD = 1800
FILENAME = os.environ.get('SCYLLA_STATUS_FILE')


class KeepAliveActivity(object):
    """A context manager which tracks entrance and exit of a block"""
    def __init__(self, name=None):
        self.name = name

    def log_start(self):
        keep_alive('Starting {}'.format(self.name) if self.name else None)

    def keep_alive(self):
        keep_alive('Continuing {}'.format(self.name) if self.name else None)

    def log_stop(self):
        keep_alive('Finished {}'.format(self.name) if self.name else None)

    def __enter__(self):
        self.log_start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.log_stop()
        return False


def keep_alive(message=None):
    """Log activity to the status file"""
    if FILENAME:
        with open(FILENAME, "w") as file:
            file.write(message or '')


def last_active():
    """Get number of seconds since last status update"""
    mtime = os.path.getmtime(FILENAME)
    return time.time() - mtime


def last_status():
    """Get the last status update message"""
    with open(FILENAME) as f:
        return f.read()


def __main():
    global FILENAME
    threshold = DEFAULT_THRESHOLD

    # process arguments
    if len(sys.argv) > 1:
        if sys.argv[1] == '--help':
            print 'args: [acceptable-seconds] [filename]'
            return 0
        threshold = int(sys.argv[1])
    if len(sys.argv) > 2:
        FILENAME = sys.argv[2]

    if not FILENAME:
        print "environment variable SCYLLA_STATUS_FILE not set"
        return 1

    # read status file
    try:
        time_ago = last_active()
        status = last_status()
    except OSError:
        print 'No status file'
        return 2

    print '{} seconds ago: {}'.format(int(time_ago), status)
    if time_ago > threshold:
        return 1
    return 0


if __name__ == '__main__':
    exit(__main())

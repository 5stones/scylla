# -*- coding: utf-8 -*-
"""functions to query the orientdb database"""
import datetime
from .. import configuration

from . import binary, rest
from .errors import OrientDbError
from .utils import encode


__config = configuration.getSection('OrientDB', {
    'slow_query_log': 10,  # set to 0 seconds to log all queries
    # 'host': 'localhost:2480',
    # 'is_ssl': True,
    # 'verify_ssl': True,
    # 'binary_port': None,  # if set, will use pyorient
    # 'db': 'Scylla',
    # 'auth': ('admin', 'admin'),
})


def execute(sql):
    """Run a SQL command and return an array of dict objects."""
    if is_binary():
        return binary.execute(sql)
    return rest.execute(sql)


def execute_batch(commands):
    """Run a list of SQL commands.

    See https://orientdb.com/docs/2.2.x/SQL-batch.html for documentation.
    """
    if is_binary():
        return binary.execute_batch(commands)
    return rest.execute_batch(commands)


def execute_transaction(commands, return_after=None):
    """Run a list of SQL commands in a transaction."""
    trans = ['begin'] + commands + ['commit retry 100']
    if return_after:
        trans.append('return {}'.format(return_after))
    return execute_batch(trans)


def is_binary():
    """If the `binary_port` config is set, then pyorient will be used instead of rest."""
    return __config.get('binary_port', None) is not None


def insert(classname, obj, return_after='@rid', also_set=''):
    """Insert a record and all subrecords.

    Args:
        classname: The name of the OrientDB class
        obj: The content of the record as a dict
        also_set: SQL string of what to set besides the content (ex: "SET _updated_at = sysdate()")
        return_after: What to return after the insert. Defaults to the record
    """
    json_data = encode(obj)
    sql = u"INSERT INTO {0} CONTENT {1} {2} RETURN {3}".format(
        classname, json_data, also_set, return_after)
    return execute(sql)


def update(rid, obj, return_after='@rid', also_set=''):
    """Updates an existing OrientDB record.

    Args:
        rid: The rid of the record
        obj: The content of the record as a dict
        also_set: SQL string of what to set besides the content (ex: "SET _updated_at = sysdate()")
        return_after: What to return after the insert. Defaults to the record
    """
    sql = u'UPDATE {} MERGE {} {} RETURN AFTER {}'.format(
        rid, encode(obj), also_set, return_after)
    result = execute(sql)

    # Orient can occasionally silently fail and return an empty result. Catch this here.
    if result is None or len(result) == 0:
        raise OrientDbError(u"Failed to update record {0} with query {1}".format(rid, sql))

    return result


def upsert(classname, obj, key_field='id', return_after='@rid', also_set=''):
    """Upserts a record and all subrecords.

    Subrecords are upserted first via Scylla.Record.get_rid() when encode is called.

    Args:
        classname: The name of the OrientDB class
        obj: The content of the record as a dict
        key_field: The field to check for an existing record
        also_set: SQL string of what to set besides the content (ex: "SET _updated_at = sysdate()")
        return_after: What to return after the insert. Defaults to the record
    """
    sql = _upsert_sql(classname, obj, key_field, return_after, also_set)
    result = execute(sql)

    # Orient can occasionally silently fail and return an empty result. Catch this here.
    if result is None or len(result) == 0:
        raise OrientDbError(u"Failed to update record {0} with query {1}".format(
            obj[key_field], sql))

    return result


def upsert_edge(from_v, to_v, classname='Updated', track_history=False, content=None):
    """Upsert an edge keeping 1 edge per pair of vertices.

    Args:
        from_v: A string rid or clause for the `FROM` of the edge
        to_v: A string rid or clause for the `TO` of the edge
        classname: The name for the edge class (if not Updated)
        content: Additional content on the edge as a dict
    """
    base_content = {'at': datetime.datetime.now()}
    if content:
        content = dict(content.items() + base_content.items())
    else:
        content = base_content
    json_data = encode(content)

    history_sql = u"ADD history = $e.at" if track_history else u""
    commands = [
        u"LET e = SELECT FROM {0} WHERE out = {1} and in = {2} ORDER BY at DESC LIMIT 1".format(
            classname, from_v, to_v),
        u"if($e.size() > 0) {",
        u"  UPDATE $e {0} MERGE {1}".format(history_sql, json_data),
        u"}",
        u"if($e.size() = 0) {",
        u"  CREATE edge {0} FROM {1} to {2} CONTENT {3}".format(classname, from_v, to_v, json_data),
        u"}",
    ]
    return execute_transaction(commands)


def create_edge(from_v, to_v, classname=None, content=None):
    """Creates an edge with an automatic `at` property.

    Args:
        from_v: A string rid or clause for the `FROM` of the edge
        to_v: A string rid or clause for the `TO` of the edge
        classname: The name for the edge class (if not E)
        content: Additional content on the edge as a dict
    """
    base_content = {'at': datetime.datetime.now()}
    if content:
        content = dict(content.items() + base_content.items())
    else:
        content = base_content

    json_data = encode(content)
    sql = u"CREATE edge {0} FROM {1} to {2} CONTENT {3}".format(classname, from_v, to_v, json_data)

    return execute(sql)


def delete_vertex(target, where=None):
    """Run a delete on a vertex or class.

    Args:
        target: A string rid or an OrientDB classname
        where: A string filter to restrict the delete to
    """
    if not where:
        sql = u"DELETE VERTEX {0}".format(target)
    else:
        sql = u"DELETE VERTEX {0} WHERE {1}".format(target, where)
    return execute(sql)


def lookup(classname, key, value, field=None, default=False):
    """Lookup a record by the value of a property.

    Args:
        classname: The name of the OrientDB class
        key: A property or list of properties to do the lookup by
        value: The value or list of values for the key or keys
        field: Instead of returning the record, return a single property of it
        default: If given, will return that value instead of raising an OrientDbError
    """
    where = _format_where(key, value)
    if field:
        projection = field + u' AS result'
    else:
        projection = ''
    sql = u"SELECT {projection} FROM {classname} WHERE {where} LIMIT 1".format(**locals())
    result = execute(sql)
    if len(result) == 0:
        if default != False:
            return default
        else:
            raise OrientDbError(u"Could not find a {classname} where {key} = {value}.".\
                format(**locals()))

    if field:
        return result[0].get('result')
    return result[0]


def find_by(target, key, value, projections=''):
    """Query by the value of a property.

    Args:
        target: The name of the OrientDB class or another `FROM` expression
        key: A property or list of properties to do the lookup by
        value: The value or list of values for the key or keys
        projections: The `SELECT` part of the query
    """
    where = _format_where(key, value)
    sql = u"SELECT {projections} FROM {target} WHERE {where}".format(**locals())
    return execute(sql)


def get(target, projections=''):
    """Load a record or run a simple query.

    Args:
        target: The rid of the OrientDB record or another `FROM` expression
        projections: The `SELECT` part of the query
    """
    sql = u"SELECT {projections} FROM {target}".format(**locals())
    return execute(sql)


def _upsert_sql(classname, obj, key_field, return_after, also_set):
    json_data = encode(obj)
    rid = obj.get('@rid')
    if rid:
        return u'UPDATE {} MERGE {} {} RETURN AFTER {}'.format(
            rid, json_data, also_set, return_after)
    return u"UPDATE {0} MERGE {1} {2} UPSERT RETURN AFTER {3} WHERE {4} = {5} LIMIT 1".format(
        classname, json_data, also_set, return_after, key_field, encode(obj[key_field]))


def _format_where(keys, values):
    if isinstance(keys, (list, tuple)):
        pairs = zip(keys, values)
    else:
        pairs = [(keys, values)]
    filters = []
    for (key, value) in pairs:
        if value == "NULL" or value == "NOT NULL":
            filters.append(u"{0} IS {1}".format(key, encode(value)))
        else:
            filters.append(u"{0} = {1}".format(key, encode(value)))
    return u' AND '.join(filters)

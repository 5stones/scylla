# -*- coding: utf-8 -*-
"""Utility functions for OrientDB data."""
import collections
import datetime
import json
import pyorient


def deep_map(func, obj, cache=None):
    """Recursively replace all items of lists, dicts, and OrientRecords."""
    # cached/visited logic (skip processed)
    if not cache:
        cache = {}
    obj_id = id(obj)
    if obj_id and obj_id in cache:
        return cache[obj_id]

    # apply function
    obj = func(obj)
    if obj_id:
        cache[obj_id] = obj

    # recursion (uses result of the function call)
    if isinstance(obj, pyorient.OrientRecord):
        deep_map(func, obj.oRecordData, cache)
    elif isinstance(obj, collections.MutableMapping):  # dict
        for (key, value) in obj.iteritems():
            obj[key] = deep_map(func, value, cache)
    elif isinstance(obj, collections.MutableSequence):  # list
        for (key, value) in enumerate(obj):
            obj[key] = deep_map(func, value, cache)

    return obj


def encode(value):
    """Encode a value as json for use in an OrientDB SQL statement."""
    return _encoder.encode(value)


def _json_default(value):
    if isinstance(value, datetime.datetime):
        date_str = value.strftime('%Y-%m-%dT%H:%M:%S%z')
        # make sure there's a timezone
        if len(date_str) == 19:
            date_str += 'Z'
        return date_str
    elif isinstance(value, Exception):
        return (value.__class__.__name__,) + value.args
    elif hasattr(value, 'get_rid'):
        return value.get_rid()
    return str(value)


_encoder = json.JSONEncoder(default=_json_default, ensure_ascii=False, encoding='utf8')

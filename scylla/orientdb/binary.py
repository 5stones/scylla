# -*- coding: utf-8 -*-
"""Binary client wrapper"""
import datetime
import pyorient
from .. import configuration

from .timed import Timed
from .record_cache import RecordCache
from .utils import deep_map


# string replacements so the commands sent through the binary protocol behave as they do for rest
_REST_SQL_REPLACEMENTS = [
    ('RETURN @rid', 'RETURN @this'),
    ('RETURN BEFORE @rid', 'RETURN BEFORE @this'),
    ('RETURN AFTER @rid', 'RETURN AFTER @this'),
]

# "Binary" formats links as tuples and requires pyorient-native
_SERIALIZATION_TYPE = pyorient.OrientSerialization.CSV

# the pyorient client once initialized
_client = None

__config = configuration.getSection('OrientDB')


def get_client():
    """Get or create the pyorient client."""
    global _client
    if not _client:
        # lazy init, since the config is set after the import
        hostname = __config['host'].split(':')[0]
        _client = pyorient.OrientDB(
            hostname, __config.get('binary_port', 2424), serialization_type=_SERIALIZATION_TYPE)
        _client.db_open(__config['db'], *__config['auth'], db_type=pyorient.DB_TYPE_GRAPH)
    return _client


def execute(sql, rest_compatible=True):
    """Run a single SQL command."""
    return _wrap_binary_call(get_client().command, sql, rest_compatible=rest_compatible)


def execute_batch(commands, rest_compatible=True):
    """Run multiple sql commands.

    See https://orientdb.com/docs/2.2.x/SQL-batch.html for documentation.
    """
    return _wrap_binary_call(
        get_client().batch, u';'.join(commands), rest_compatible=rest_compatible)


def _wrap_binary_call(func, sql, limit=None, fetchplan=None, rest_compatible=True):
    """Handles fetchplan callback and rest compatiblity of a pyorient call."""
    if rest_compatible:
        sql = _from_rest_sql(sql)

    subrecords = RecordCache()
    with Timed(u'OrientDB query: `{}`'.format(sql), __config.get('slow_query_log')):
        results = func(sql, limit, fetchplan, subrecords.add)
    results = subrecords.populate_links(results)

    if rest_compatible:
        results = deep_map(_rest_convert, results)

    return results


def _rest_convert(data):
    if isinstance(data, pyorient.OrientRecord):
        data = _to_dict(data)
    elif isinstance(data, pyorient.OrientRecordLink):
        data = data.get_hash().replace('##', '#')
    elif isinstance(data, datetime.datetime):
        data = data.strftime('%Y-%m-%dT%H:%M:%S%z')
        # make sure there's a timezone
        if len(data) == 19:
            data += 'Z'
    elif isinstance(data, datetime.date):
        data = data.isoformat()

    # keys and values to unicode
    return _as_unicode(data)


def _as_unicode(data):
    if isinstance(data, str):
        return unicode(data, 'utf-8')
    elif isinstance(data, dict):
        for (key, value) in data.iteritems():
            if isinstance(key, str):
                del data[key]
                key = unicode(key, 'utf-8')
                data[key] = value
    return data


def _from_rest_sql(sql):
    for replacements in _REST_SQL_REPLACEMENTS:
        sql = sql.replace(*replacements)
    return sql


def _to_dict(orecord):
    if isinstance(orecord, pyorient.OrientRecord):
        data = dict(orecord.oRecordData)
        data[u'@class'] = orecord._class
        data[u'@rid'] = orecord._rid
        data[u'@version'] = orecord._version
        data[u'@type'] = u'd'
        return data
    return orecord

# -*- coding: utf-8 -*-
"""Rest client wrapper"""
import requests
from .. import configuration

from . import errors
from .utils import encode
from .timed import Timed


__config = configuration.getSection('OrientDB')


def execute(sql):
    """Run a single SQL command."""
    with Timed(u'OrientDB query: `{}`'.format(sql), __config.get('slow_query_log')):
        response = _post('command/{db}/sql', sql)

    if response.status_code != 200:
        raise errors.OrientDbRestError(response.text, sql)

    results = response.json(encoding='utf8')['result']
    return _process_response(results)


def execute_batch(commands):
    """Run multiple sql commands.

    See https://orientdb.com/docs/2.2.x/SQL-batch.html for documentation.
    """
    data = {
        "transaction" : True,
        "operations" : [
            {
                "type" : "script",
                "language" : "sql",
                "script" : commands
            }
        ]
    }
    with Timed(u'OrientDB batch: `{}`'.format(commands), __config.get('slow_query_log')):
        response = _post('batch/{db}', encode(data))

    if response.status_code != 200:
        raise errors.OrientDbRestError(response.text, commands)

    results = response.json(encoding='utf8')['result']
    return _process_response(results)


def _post(path, data):
    auth = __config['auth']
    verify = __config.get('verify_ssl', True)
    return requests.post(
        _get_url(path), data.encode(encoding='utf8'), auth=auth, verify=verify)


def _get_url(path):
    return ('{proto}://{host}/' + path).format(
        proto=('https' if __config.get('is_ssl', True) else 'http'),
        **__config)


def _process_response(results):
    """Does post processing on results.

    Currently, it only is used to link up repeated objects in results.
    """
    rids = {}
    objs = []
    _get_records(results, rids, objs)
    for obj in objs:
        _process_record(obj, rids)
    return results


def _get_records(results, rids, objs):
    """Fill rids dict with all records in the results."""
    for value in results:
        if isinstance(value, dict):
            if '@rid' in value:
                objs.append(value)
                if value['@rid'][1] != '-':
                    rids[value['@rid']] = value
            _get_records(value.values(), rids, objs)
        elif isinstance(value, list):
            _get_records(value, rids, objs)


def _process_record(record, rids):
    """Processes a record and converts data types."""
    try:
        # the @fieldtypes property tells you how to interpret the results
        fieldtypes = [tuple(f.split('=')) for f in record['@fieldTypes'].split(',')]
        for (field, o_type) in fieldtypes:
            _conv_prop(record, field, o_type, rids)
    except KeyError:
        pass


def _conv_prop(record, field, o_type, rids):
    """Converts a single field using a type."""
    if o_type == 'a': # date
        pass
    elif o_type == 't': # datetime
        pass
    elif o_type == 'x': # link
        if isinstance(record[field], basestring) and record[field] in rids:
            record[field] = rids[record[field]]
    elif o_type in ['n', 'z', 'g', 'm']:  # link set, list, bag, or map
        for (i, value) in enumerate(record[field]):
            if isinstance(value, basestring) and value in rids:
                record[field][i] = rids[value]

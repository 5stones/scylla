# -*- coding: utf-8 -*-
"""Cache for OrientDB Records."""
import pyorient
from .utils import deep_map


class RecordCache(dict):
    """Cache of rid => Record, with a helper to populate references with loaded records."""

    def add(self, rec):
        """Add a record by rid (used for callback on command/query)."""
        self[rec._rid] = rec

    def populate_links(self, results):
        """Replace record links with cached records."""
        return deep_map(self._map_value, results)

    def _map_value(self, obj):
        """Replace a link or cache a record."""
        if isinstance(obj, pyorient.OrientRecordLink) and obj.get_hash() in self:
            return self[obj.get_hash()]
        elif isinstance(obj, pyorient.OrientRecord):
            self[obj._rid] = obj
        return obj

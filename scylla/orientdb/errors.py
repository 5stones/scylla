# -*- coding: utf-8 -*-
"""Errors related to OrientDB"""

class OrientDbError(Exception):
    """Errors related to OrientDB and its results."""
    pass


class OrientDbRestError(OrientDbError):
    """Error during an http request to OrientDB."""
    pass

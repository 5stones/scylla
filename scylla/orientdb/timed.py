# -*- coding: utf-8 -*-
import logging
import time

class Timed(object):
    """A controlled execution which will time a process, and log
    a message if over a threshold in seconds.
    """
    def __init__(self, message, threshold=0):
        self.message = message
        self.threshold = threshold
        self.start = None
        self.delta = None

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, type, value, traceback):
        self.delta = (time.time() - self.start)
        if self.delta >= self.threshold:
            logging.warning(u"%f second %s", self.delta, self.message)

#!/usr/bin/python2
import datetime
import logging
import pyorient


class ReflectionCleaner(object):
    """Removes old Updated edges to prevent performance issues."""

    def __init__(self, config, retain_count=100):
        self.client = self._create_client(config)
        self.retain_count = retain_count

    def clean(self, clazz, date_field='_updated_at', delta=datetime.timedelta(days=7)):
        params = {
            'date_field': date_field,
            'class': clazz,
            'retain': self.retain_count,
        }
        results = self._command('SELECT min({date_field}), max({date_field}) FROM {class}', params)
        dates = results[0]

        page_query = ('SELECT @rid as rid '
                      'FROM {class} '
                      'WHERE {date_field} BETWEEN "{start}" AND "{end}" '
                      '  AND out("Updated").size() > {retain}')

        for (start, end) in self._periods(dates.min, dates.max, delta):
            logging.info(u"-- %s--%s", start, end)
            params['start'] = start
            params['end'] = end
            results = self._command(page_query, params)
            if results:
                for result in results:
                    try:
                        self._clean_old(result.rid.get_hash())
                    except pyorient.exceptions.PyOrientCommandException:
                        logging.exception("Could not clean edges for record: {}".format(result.rid.get_hash()))

    def _clean_old(self, rid):
        rows = self._command(
            'SELECT distinct(in) AS result FROM Updated WHERE out = "{rid}"',
            {'rid': rid})
        for row in rows:
            self._del_old({'out': rid, 'in': row.result})

    def _del_old(self, pair):
        query = ('SELECT at '
                 'FROM Updated '
                 'WHERE out = {out} and in = {in} '
                 'ORDER BY at desc SKIP {retain} LIMIT 1').format(retain=(self.retain_count - 1), **pair)
        results = self._command(query)
        if results and results[0].at:
            del_cmd = 'DELETE EDGE Updated WHERE out = {out} AND in = {in} AND at < "{oldest}"'.format(
                oldest=results[0].at, **pair)
            self._command(del_cmd)

    def _command(self, cmd, args=None):
        params = args or {}
        formatted_cmd = cmd.format(**params)
        logging.info('Running: %s', formatted_cmd)
        return self.client.command(formatted_cmd)

    def _batch(self, commands):
        return self.client.batch(';'.join(commands))

    @staticmethod
    def _periods(start_date, end_date, delta):
        start = start_date
        while start < end_date:
            end = start + delta
            yield (start, end)
            start = end

    @staticmethod
    def _create_client(config):
        hostname = config['host'].split(':')[0]
        client = pyorient.OrientDB(hostname, config.get('binary_port', 2424))
        client.db_open(config['db'], *config['auth'], db_type=pyorient.DB_TYPE_GRAPH)
        return client


def __main():
    import sys
    # assume config.py exists in path
    import config

    if len(sys.argv) < 2:
        print('Argument for class to cleanup required')
        print('Usage: python -m scylla.orientdb.reflection_cleaner CLASS [RETAIN_COUNT]')
        exit(1)

    retain_count = int(sys.argv[2]) if len(sys.argv) == 3 else 100

    logging.basicConfig(level=10)
    cleaner = ReflectionCleaner(config.settings['OrientDB'], retain_count)
    cleaner.clean(sys.argv[1])


if __name__ == '__main__':
    __main()

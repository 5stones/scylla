import requests
from requests.packages import urllib3


class RestClient(object):
    _successful_codes = [200, 201, 204]

    class RestError(Exception):
        pass

    def __init__(self, base_url, verify_ssl=True, debug=False):
        self.base_url = base_url
        self.debug = debug
        self._base_request = {
            'headers': {
                #'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            'verify': verify_ssl,
        }
        if not verify_ssl:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _process_response(self, response):
        if response.status_code not in self._successful_codes:
            raise self.RestError(response.text)
        return response.json()

    def _set_header(self, header, value):
        self._base_request['headers'][header] = value

    def get(self, path, params=None):
        if self.debug:
            print('GET '+path, params)
        response = requests.get(
            self.base_url + path, params=params,
            **self._base_request )
        return self._process_response(response)

    def post(self, path, params=None, data=None, wrap_keys=None):
        enc_data = self._encode_data(data, wrap_keys)
        if self.debug:
            print('POST '+self.base_url+path, params, enc_data, self._base_request)
        response = requests.post(
            self.base_url + path, params=params,
            data=enc_data,
            **self._base_request )
        return self._process_response(response)

    def put(self, path, params=None, data=None, wrap_keys=None):
        enc_data = self._encode_data(data, wrap_keys)
        if self.debug:
            print('PUT '+path, params, enc_data)
        response = requests.put(
            self.base_url + path, params=params,
            data=enc_data,
            **self._base_request )
        return self._process_response(response)

    def delete(self, path, params=None):
        if self.debug:
            print('DELETE '+path, params)
        response = requests.delete(
            self.base_url + path, params=params,
            **self._base_request )
        return self._process_response(response)

    @staticmethod
    def _encode_data(data, wrap_keys=None):
        if not data:
            return None
        if wrap_keys:
            return dict([('{0}[{1}]'.format(wrap_keys, k), v) for (k, v) in data.items()])
        return data

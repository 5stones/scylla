from decimal import Decimal
from . import orientdb
from . import convert


class ClassInitMetaclass(type):
    """Runs the __class_init__(cls) classmethod after definition.
    """
    def __init__(self, name, bases, attrs):
        try:
            self.__class_init__()
        except AttributeError:
            # no __class_init__ defined
            pass
        return type.__init__(self, name, bases, attrs)


class LazyInitMetaclass(ClassInitMetaclass):
    """Runs the __class_init__(cls) classmethod after definition,
    as well as the __class_lazy_init__(cls) classmethod when used.
    """
    def __init__(self, name, bases, attrs):
        self.__initialized = None
        super(LazyInitMetaclass, self).__init__(name, bases, attrs)
        self.__initialized = False

    def __initialize(self):
        if type.__getattribute__(self, '_LazyInitMetaclass__initialized') == False:
            self.__initialized = True
            try:
                type.__getattribute__(self, '__class_lazy_init__')()
            except AttributeError:
                pass

    def __getattribute__(*args):
        self = args[0]
        type.__getattribute__(self, '_LazyInitMetaclass__initialize')()
        return type.__getattribute__(*args)

    def __call__(self, *args, **kwargs):
        self.__initialize()
        return super(LazyInitMetaclass, self).__call__(*args, **kwargs)


class Record(object):
    """The basic record which can be saved into OrientDB.

    Attributes:
        data: A dict containing the data for the record.
        rid: The OrientDB record id like "#12:5".
    """
    __metaclass__ = LazyInitMetaclass
    key_field = 'id'
    classname = 'V'
    _readonly_link = False

    def __init__(self, data):
        """
        Args:
            data: A dict of the data for this record.
        """
        self.data = data
        if '@class' in data:
            self.classname = data['@class']
        if '@rid' in data:
            self.rid = data['@rid']
        else:
            self.rid = None

    def get(self, key, default=None):
        try:
            return self.data[key]
        except KeyError:
            return default

    def __getitem__(self, key):
        return self.data[key]

    def throw_at_orient(self):
        result = orientdb.upsert(self.classname, self.data, self.key_field,
                return_after='@rid',
                also_set='SET _updated_at = date()')
        self.rid = result[0]['@rid']
        return result[0]

    def _query_rid(self):
        try:
            self.rid = orientdb.lookup(
                self.classname,
                self.key_field,
                self.get(self.key_field),
                '@rid')
        except Exception:
            pass
        return self.rid

    def get_rid(self):
        if self._readonly_link and not self.rid:
            self._query_rid()
        if not self.rid:
            self.throw_at_orient()
        return self.rid


class ParsedRecord(Record):
    key_field = 'id'
    classname = 'Record'

    # what python classes will start with (usually the connection name)
    factory_base_classname = ''
    # dict of field to classname, of what subrecords to automatically update
    _sub_records = {}
    # dict of field to classname, of what subrecords to create and link but not update
    _linked_records = {}

    # list of fieldnames to parse as ISO datetimes
    _datetime_fields = []
    # list of fieldnames to parse as decimals
    _decimal_fields = []
    # list of fieldnames to parse as integers
    _int_fields = ['id']

    # recursion depth when initializing records
    __process_depth = 0

    # cache of records cleared after processing a response
    __record_cache = {}

    # this record's base record type for searching for subclasses
    _record_base_cls = None

    @classmethod
    def factory(cls, client, classname, obj, readonly_link=False):
        """Instantiates a ParsedRecord object using a subclass if one is available.

        Args:
            classname: The name of the class to append to the basename, which
                will be the OrientDB classname.
            obj: A dict or list that contains the data for this record(s),
                and its subrecords.
        """
        if obj is None:
            return None

        # search for a record type from a module's base record type
        base_cls = cls._record_base_cls or cls
        subcls = base_cls.get_subclass(classname)

        # subcls.factory may be called, so make it search from the same base
        subcls._record_base_cls = base_cls

        if isinstance(obj, Record):
            return obj
        elif isinstance(obj, dict):
            return subcls.process_obj(client, classname, obj, readonly_link)
        elif isinstance(obj, list):
            return [subcls.process_obj(client, classname, d, readonly_link) for d in obj]
        else:
            raise Exception('Subclass data must be a list or dict: '+str(obj))

    @classmethod
    def get_subclass(cls, classname):
        '''Get a subclass by classname or return this class.

        Args:
            classname: The name of the class to append to the basename, which
                will be the OrientDB classname.
        '''
        base_classname = cls.factory_base_classname or ''
        python_classname = '{}{}'.format(base_classname, classname)
        for subclass in cls._get_subclasses():
            if subclass.__name__ == python_classname:
                return subclass
        return cls

    @classmethod
    def _get_subclasses(cls):
        """Recursively find subclasses and subclasses of subclasses"""
        subclasses = []
        for subclass in cls.__subclasses__():
            subclasses.append(subclass)
            subclasses.extend(subclass._get_subclasses())
        return subclasses

    def __init__(self, client, classname, obj, readonly_link=False):
        super(ParsedRecord, self).__init__(obj)
        self.client = client
        self._readonly_link = readonly_link
        self.module_name = client.name
        if classname:
            self.classname = self.module_name + classname

    @classmethod
    def process_obj(cls, client, classname, obj, readonly_link=False):
        if isinstance(obj, Record):
            # it's already converted
            return obj
        elif isinstance(obj, basestring) and obj[0] == '#':
            # this is just a string, so it's probably an RID because of a circular reference
            try:
                return cls.__record_cache[obj]
            except KeyError:
                return obj

        # check record cache (creates circular reference instead of an infinite loop)
        if '@rid' in obj and obj['@rid'] in cls.__record_cache:
            return cls.__record_cache[obj['@rid']]

        # initialize the record
        rec = cls(client, classname, obj, readonly_link)

        # cache the record
        if '@rid' in obj:
            cls.__record_cache[obj['@rid']] = rec

        # process the fields and initialize subrecords
        cls.__process_depth += 1
        rec._process_fields()
        cls.__process_depth -= 1

        # if we finished processing all records, clear our cache
        if cls.__process_depth == 0:
            cls.__record_cache.clear()

        return rec

    def _process_fields(self):
        #remove any invalid fields from the dict
        if "" in self.data:
            self.data.pop("", None)

        # create sub records (updates the record during save)
        for (field, classname) in self._sub_records.items():
            if field in self.data:
                self.data[field] = self.factory(self.client, classname, self.data[field])

        # create linked records
        for (field, classname) in self._linked_records.items():
            if field in self.data:
                self.data[field] = self.factory(self.client, classname, self.data[field], True)

        # process datetimes
        for field in self._datetime_fields:
            if field in self.data:
                self.data[field] = convert.parse_iso_date(self.data[field])

        # decimals
        for field in self._decimal_fields:
            if field in self.data and not isinstance(self[field], Decimal):
                try:
                    self.data[field] = Decimal(str(self[field]))
                except:
                    self.data[field] = None

        # ints
        for field in self._int_fields:
            if field in self.data and not isinstance(self[field], int):
                try:
                    self.data[field] = int(self[field])
                except (ValueError, TypeError):
                    self.data[field] = None

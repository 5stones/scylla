import os
import inspect
import collections
from time import sleep
from sys import argv
from getopt import getopt
from daemon import runner
from . import orientdb
from . import configuration
from . import stats
from . import graceful


class App(object):
    """A collection of Tasks which can be run repeatedly as a daemon.

    Attributes:
        rest_length: Number of seconds to rest between each task.
        nap_length: Number of seconds to rest after finishing all tasks.
        retry_error_interval: The ratio of running through tasks to retrying errors.
        help_message: The description of the task.
    """

    rest_length = 1
    nap_length = 60
    retry_error_interval = 10
    help_message = None

    main_options = [
        ('after', 'a', None),
        ('retry-errors', 'r'),
    ]

    _instance = None

    def __init__(self, config):
        self.__merge_config(config)
        self.tasks = collections.OrderedDict()
        self.on_demand_tasks = {}
        self.other_commands = collections.OrderedDict([
            ('help', self.print_help),
            ('step', self.step),
            ('retry-errors', self.process_errors),
            ('stats', stats.print_metrics),
            ('summary', stats.summary),
        ])

        self._prepare()

    @staticmethod
    def __merge_config(config):
        for (name, props) in config.items():
            configuration.updateSection(name, props)

    def _prepare(self):
        pass

    def run(self):
        retry_error_counter = 0
        while True:
            self.step()

            retry_error_counter += 1
            if retry_error_counter >= self.retry_error_interval:
                self.process_errors()
                sleep(self.rest_length)
                retry_error_counter = 0

            sleep(self.nap_length)

    def step(self):
        """Runs through all tasks once."""
        for (name, task) in self.tasks.items():
            task()
            sleep(self.rest_length)

    def process_errors(self, limit=None):
        """Finds errors linked to each task and retries them"""
        for task in self.tasks.values():
            if limit:
                task.retry_record_errors(limit)
            else:
                task.retry_all_record_errors()

    def get_task(self, record_type):
        if record_type in self.tasks:
            return self.tasks[record_type]
        if record_type in self.on_demand_tasks:
            return self.on_demand_tasks[record_type]

        record_type = record_type.title()
        return self.tasks[record_type]

    def daemon_action(self):
        config = configuration.getSection('Daemon')
        daemon = Daemon(self, **config)
        daemon_runner = runner.DaemonRunner(daemon)
        daemon_runner.do_action()

    @staticmethod
    def _get_help(obj):
        message = getattr(obj, 'help_message', None)
        if not message:
            message = inspect.getdoc(obj)
        try:
            return message.split('\n', 1)[0]
        except AttributeError:
            return ''

    def print_help(self, args, options):
        '''Prints a help message for the app.'''
        command = argv[0]
        print('Usage: {} [options] [task|action|command] [arguments]'.format(command))
        print(self._get_help(self))

        if len(self.tasks):
            print('\nTasks:')
            for (task_name, task) in self.tasks.iteritems():
                print('  {:34}\t{}'.format(task_name, self._get_help(task)))

        print('\nOptions for tasks:')
        parser = OptionParser(self.main_options)
        parser.print_options()

        if len(self.on_demand_tasks):
            print('\nTasks run on demand:')
            for (task_name, task) in self.on_demand_tasks.iteritems():
                print('  {:34}\t{}'.format(task_name, self._get_help(task)))

        if len(self.other_commands):
            print('\nCommands:')
            for (task_name, task) in self.other_commands.iteritems():
                print('  {:34}\t{}'.format(task_name, self._get_help(task)))

        print('\nDaemon Actions:')
        for action in ['start', 'stop', 'restart']:
            print('  '+action)

        print('\nExamples:')
        print('  {:36}  # starts running in the foreground'.format(
            command))
        print('  {:36}  # starts running in the background as a daemon'.format(
            command + ' start'))
        print('  {:36}  # run through all tasks once'.format(
            command + ' step'))
        print('  {:36}  # runs a task once'.format(
            command + ' TASK'))
        print('  {:36}  # retry errors for all tasks'.format(
            command + ' retry-errors'))
        print('  {:36}  # retry errors for a specific task'.format(
            command + ' --retry-errors TASK'))

    def main(self):
        graceful.GracefulShutdown.init_handlers()

        parser = OptionParser(self.main_options)
        (options, args) = parser.parse()

        if len(args) == 0:
            self.run()
        elif len(args) == 1 and args[0] in ['start', 'stop', 'restart']:
            self.daemon_action()
        elif len(args) == 1 and args[0] in ['retry-errors', 'step']:
            self.other_commands[args[0]]()
        elif len(args) >= 1 and args[0] in self.other_commands:
            self.other_commands[args[0]](args[1:], options)
        else:
            task = self.get_task(args[0])
            ids = args[1:]

            if options['retry-errors']:
                task.retry_all_record_errors()
            elif len(ids):
                task.process_ids(ids)
            else:
                task(options)


class Daemon(object):
    """Runs an App as a Daemon.
    This is used from App, and doesn't need to be accessed directly.
    """
    base_path = os.path.dirname(os.path.abspath(argv[0]))

    def __init__(self, app, name=None, pid_dir='run', log_dir='log'):
        self.app = app

        if not name:
            name = app.__class__.__module__
        self.__configure_paths(pid_dir, log_dir, name)

    def __configure_paths(self, pid_dir, log_dir, daemon_name):
        pid_dir = self.__check_dir(pid_dir)
        log_dir = self.__check_dir(log_dir)

        self.pidfile_path = "{pid_dir}/{daemon_name}.pid".format(**locals())
        self.pidfile_timeout = 5
        self.stdin_path = '/dev/null'
        self.stdout_path = "{log_dir}/{daemon_name}.log".format(**locals())
        self.stderr_path = "{log_dir}/{daemon_name}.err".format(**locals())

    def __check_dir(self, path):
        if path[0] != '/':
            path = "{0}/{1}".format(self.base_path, path)
        if not os.path.exists(path):
            os.makedirs(path, 0775)
        return path

    def run(self):
        self.app.run()


class OptionParser(object):
    """Parser for command line arguments: flags, options, args.
    """
    def __init__(self, opts = None):
        self.short_opts = ""
        self.long_opts = []
        self.flags = []
        self.options = []
        if opts:
            self.__add_options(opts)

    def __add_options(self, opts):
        for opt in opts:
            if len(opt) == 3:
                self.add_option(*opt)
            else:
                self.add_flag(*opt)

    def add_flag(self, long_name, short_name):
        self.flags.append((long_name, short_name))
        self.short_opts += short_name
        self.long_opts.append(long_name)

    def add_option(self, long_name, short_name, default=None):
        self.options.append((long_name, short_name, default))
        self.short_opts += "{0}:".format(short_name)
        self.long_opts.append("{0}=".format(long_name))

    def print_options(self):
        for flag in self.flags:
            print('  -{}, --{}'.format(flag[1], flag[0]))
        for option in self.options:
            print('  -{}, --{}=VALUE\tdefault={}'.format(option[1], option[0], option[2] or ''))

    def parse(self):
        (opt_tuples, args) = getopt(argv[1:], self.short_opts, self.long_opts)
        opt_dict = dict(opt_tuples)

        options = {}
        # parse the flags
        for (long_name, short_name) in self.flags:
            short_opt = '-{0}'.format(short_name)
            long_opt = '--{0}'.format(long_name)
            if short_opt in opt_dict:
                options[long_name] = True
            elif long_opt in opt_dict:
                options[long_name] = True
            else:
                options[long_name] = False

        # parse the options with parameters
        for (long_name, short_name, default) in self.options:
            short_opt = '-{0}'.format(short_name)
            long_opt = '--{0}'.format(long_name)
            options[long_name] = opt_dict.get(long_opt, opt_dict.get(short_opt, default))

        return (options, args)

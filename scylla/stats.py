from . import orientdb


def print_metrics(args, options):
    '''Prints various numeric metrics.'''
    if options.get('after') is not None:
        # after the specified time
        after = "'{}'".format(options['after'] or '0000-00-00')
    else:
        # in the last 5 minutes
        after = 'eval("sysdate() - 60000 * 5").asDatetime()'

    stats = {
        'Edges': (
            'SELECT COUNT(*) AS result '
            'FROM Reflection '
            'WHERE at >= ' + after),

        'Tasks': (
            'SELECT COUNT(*) AS result '
            'FROM Task '
            'WHERE last_run >= ' + after),

        'RecordErrors': (
            'SELECT COUNT(*) AS result '
            'FROM RecordError '
            'WHERE created_at >= ' + after),

        'Other Errors': (
            'SELECT COUNT(*) AS result '
            'FROM cluster:error '
            'WHERE created_at >= ' + after),

        'RecordError Total': (
            'SELECT COUNT(*) AS result '
            'FROM RecordError '),
    }

    if len(args) and args[0]:
        stat = args[0]
        print(orientdb.execute(stats[stat])[0]['result'])
    else:
        for (stat, query) in stats.iteritems():
            print('{}: {}'.format(stat, orientdb.execute(stats[stat])[0]['result']))


def summary(args, options):
    '''Prints a summary of tasks and records.'''
    if options.get('after') is not None:
        # after the specified time
        after = "'{}'".format(options['after'] or '0000-00-00')
    else:
        # in the last 24 hours
        after = 'sysdate() - 86400000'

    _print_task_summary(after)
    print('')
    _print_edge_summary(after)


def _get_edge_summary(after):
    return orientdb.execute((
        'SELECT out.@class AS from, in.@class AS to, '
        '  count(if(eval("@class = \'Created\'"), true, null)) AS created, '
        '  count(if(eval("@class = \'Updated\'"), true, null)) AS updated '
        'FROM Reflection '
        'WHERE at > eval("({0})").asDate() '
        'GROUP BY out.@class, in.@class ').format(after))


def _get_task_summary(after):
    return orientdb.execute((
        'SELECT id, last_run, '
        '  eval("last_run > {0}") AS was_run, '
        '  $errors.today AS new_errors, '
        '  $errors.total AS total_errors '
        'FROM Task '
        'LET '
        '  $task = @this, '
        '  $errors = first(('
        '    SELECT '
        '      count(if(eval("created_at > {0}"), true, null)) as today, '
        '      count(*) AS total '
        '    FROM RecordError '
        '    WHERE task = $task)) '
        'WHERE last_run > eval("date() - 30 * 86400000").asDate() '
        'ORDER BY id ').format(after))


def _print_edge_summary(after):
    results = _get_edge_summary(after)

    print("\n# Records #\n")
    f = '{:>7} + {:>7} | {:>20} > {}'
    print(f.format('Created', 'Updated', 'From Record', 'To Record'))
    print('---------------------------------------------------------------')
    for row in results:
        print(f.format(row['created'], row['updated'], row['from'], row['to']))


def _print_task_summary(after):
    results = _get_task_summary(after)

    up = True
    for row in results:
        if not row['was_run']:
            up = False

    if up:
        print("PASSED status check.\n")
    else:
        print("FAILED status check:\n- At least one task didn't run successfully.\n")

    print("\n# Tasks #\n")
    f = '{:>3} | {:<20} | {:>5} / {:>12} | {}'
    print(f.format('Up', 'Last run time', 'New', 'Total Errors', 'Task'))
    print('-------------------------------------------------------------------------------------------------')
    for row in results:
        print(f.format(row['was_run'], row.get('last_run', ''),
            row['new_errors'], row['total_errors'], row['id']))

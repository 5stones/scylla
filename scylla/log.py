# coding: utf8
import logging
import json
import sys
import traceback
from . import orientdb
from . import status
from . import configuration
from .records import Record


__config = configuration.getSection('OrientDB', {
    'persist_errors': True,
})


class OneLineFormatter(logging.Formatter):
    """Formats logs to be on oneline with exceptions as json."""

    stdout = None

    @classmethod
    def configure_stdout(cls, level=logging.INFO):
        """Configured the root logger to this formatter."""
        if not cls.stdout:
            cls.stdout = logging.StreamHandler()
            logging.getLogger().addHandler(cls.stdout)
        logging.getLogger().setLevel(level)
        cls.stdout.setFormatter(OneLineFormatter('%(levelname)-8s "%(message)s"'))
        return cls.stdout

    def format(self, record):
        return super(OneLineFormatter, self).format(record).replace('\n', ' ')

    def formatException(self, ei):
        """Formats exc_info as a json object."""
        return json.dumps({
            'exception': {
                'type': ei[0].__name__,
                'message': unicode(ei[1].message),
                'trace': traceback.extract_tb(ei[2]),
            },
        })


OneLineFormatter.configure_stdout()


def print_error(err):
    """Helper for logging errors."""
    if isinstance(err, RecordError):
        logging.warn(err, exc_info=True)
    else:
        logging.error(err, exc_info=True)


class ErrorLogging(status.KeepAliveActivity):
    """A controlled execution where errors will be caught and logged to the
    database.
    """
    def __init__(self, task_id=None, rec=None):
        self.task_id = task_id
        if rec and not isinstance(rec, Record):
            rec = Record(rec)
        self.rec = rec

        name = task_id or 'N/A'
        if rec:
            name += u': {} {}'.format(rec.classname, rec.get(rec.key_field))
        super(ErrorLogging, self).__init__(name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(ErrorLogging, self).__exit__(exc_type, exc_val, exc_tb)
        if exc_val is not None:
            return self._failure(exc_type, exc_val, exc_tb)
        self._success()
        return False

    def _success(self):
        if self.rec:
            RecordError.delete_for(self.task_id, self.rec)

    def _failure(self, exc_type, exc_val, exc_tb):
        if isinstance(exc_val, Exception):
            error(exc_val, self.task_id, self.rec)
            return True
        return False


def error(err, task_id=None, rec=None):
    """Logs an error to the database.
    """
    if rec:
        err_rec = RecordError(err, task_id, rec)
    else:
        err_rec = ErrorLog(err)

    also_set = u"SET created_at = IFNULL(created_at, date()), last_attempted_at = date() "

    # link task
    if task_id:
        also_set += u", task = first((SELECT FROM Task WHERE id = '{0}' LIMIT 1))".format(task_id)

    if isinstance(err_rec, RecordError) or __config.get('persist_errors', True):
        err_rec.save(also_set)

    print_error(err_rec)


class ErrorLog(dict):
    classname = 'Error'

    def __init__(self, err):
        if isinstance(err, dict):
            # init parent with dictionary
            super(ErrorLog, self).__init__(err)
        else:
            # init empty parent dictionary
            super(ErrorLog, self).__init__()

            # fill in dictionary with inforation on the error
            if isinstance(err, Exception):
                self['exception'] = err
                self['trace'] = self.get_last_traceback()
                try:
                    self['message'] = u'{0} - {1}'.format(err.__class__.__name__, err)
                except TypeError:
                    self['message'] = err.__class__.__name__
            else:
                self['message'] = err

            # fill in details if there are any
            try:
                self['details'] = err.details
            except AttributeError:
                pass

    @staticmethod
    def get_last_traceback():
        tb = sys.exc_info()[2]
        if tb:
            return traceback.extract_tb(tb)
        return None

    def __str__(self):
        if 'message' in self:
            return self['message'].encode(encoding='utf-8')
        else:
            return super(ErrorLog, self).__str__()

    def save(self, also_set=''):
        orientdb.insert(self.classname, self, also_set=also_set)
        # maybe we should draw an edge to the task


class RecordError(ErrorLog):
    classname = 'RecordError'

    def __init__(self, err, task_id, rec):
        super(RecordError, self).__init__(err)
        self.with_rec(task_id, rec)

    @staticmethod
    def generate_id(task_id, rec):
        return u"{} {} {}".format(task_id, rec.classname, rec.get(rec.key_field))

    @classmethod
    def delete_for(cls, task_id, rec):
        orientdb.delete_vertex(cls.classname, u"id = '{}'".format(cls.generate_id(task_id, rec)))

    def with_rec(self, task_id, rec):
        self['id'] = self.generate_id(task_id, rec)
        self['document_ref'] = (rec.classname, rec.get(rec.key_field))
        self['message'] = u"Error processing {0}:{1}: {2}".format(
            rec.classname, rec.get(rec.key_field), self['message'])
        if rec.rid and rec.rid[:2] != '#-':
            self['document'] = rec.rid

    def save(self, also_set=''):
        orientdb.upsert(self.classname, self, 'id', also_set=also_set)

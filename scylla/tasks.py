# coding: utf8
from datetime import datetime
import logging
from . import orientdb
from . import log
from . import graceful


class TaskContext(log.ErrorLogging):
    """Handles errors and tracks the last time something ran successfully.
    """
    def __init__(self, task_id=None, rec=None, module=None, timing_buffer=0):
        super(TaskContext, self).__init__(task_id, rec)
        self.module = module
        self.start_time = None
        self.timing_buffer = timing_buffer
        self._last_run_time = None

    def __enter__(self):
        super(TaskContext, self).__enter__()
        self.start()
        return self

    def _success(self):
        super(TaskContext, self)._success()
        self.last_run_time = self.start_time

    def start(self):
        """Log the start of a task run, and load the last_run_time"""
        self.start_time = datetime.utcnow()
        task_obj = {
            'id': self.task_id,
            'module': self.module,
            'started_at': self.start_time,
        }
        result = orientdb.upsert('Task', task_obj,
                                 return_after='@this.last_run.asLong()')
        self._last_run_time = self._parse_last_run_ms(result[0]['value'])

    @property
    def last_run_time(self):
        """Retrieve the last time this task ran sucessfully"""
        if self._last_run_time is None:
            last_run_ms = orientdb.lookup('Task', 'id', self.task_id, 'last_run.asLong()', None)
            self._last_run_time = self._parse_last_run_ms(last_run_ms)
        return self._last_run_time or None

    @last_run_time.setter
    def last_run_time(self, value):
        """Save value as the timestamp of the last successful run"""
        orientdb.upsert('Task', {'id':self.task_id, 'last_run':value})
        self._last_run_time = value

    def _parse_last_run_ms(self, last_run_ms):
        if last_run_ms:
            last_run_adjusted = datetime.utcfromtimestamp(last_run_ms/1000 - self.timing_buffer)
            return last_run_adjusted.isoformat() + 'Z'
        return ''


class Task(object):
    """A task or job which tracks the last successful run.
    This is just a template and doesn't need to be used.
    """
    timing_buffer = 1
    help_message = ''

    def __init__(self, task_id, module=None):
        self.context = self._create_context(task_id, module)

    def __call__(self, options=None):
        """Runs one step of the task.
        """
        if options is None:
            options = {}
        try:
            with self.context:
                after = options.get('after', None) or self.context.last_run_time
                self._step(after, options)
        except orientdb.OrientDbError:
            logging.critical('Could not run task %s', self.task_id, exc_info=True)

    def _step(self, after, options):
        raise NotImplementedError()

    @property
    def task_id(self):
        """The id of the Task record"""
        return self.context.task_id

    @task_id.setter
    def task_id(self, value):
        """Recreates the task context with the new task id"""
        self.context = self._create_context(value, self.scylla_module)

    @property
    def scylla_module(self):
        """The scylla module of the Task"""
        return self.context.module

    @scylla_module.setter
    def scylla_module(self, value):
        """Recreates the task context with the new module name"""
        self.context = self._create_context(self.task_id, value)

    def _create_context(self, task_id, module):
        if not module:
            module = self._determine_module()
        return TaskContext(
            task_id,
            module=module,
            timing_buffer=self.timing_buffer)

    @classmethod
    def _determine_module(cls):
        name = cls.__module__.split('.')[0]
        if not name.startswith('scylla_'):
            return None
        return name[7:].replace('_', '-')

    def retry_record_error(self, error):
        """Retry one RecordError pulled from OrientDB.
        """
        with log.ErrorLogging(self.task_id):
            (record_type, record_id) = error['document_ref']
            self.process_one(record_id)

    def retry_record_errors(self, limit=100):
        """Retries errors up to a limit starting with the ones least recently attempted.
        """
        errors = orientdb.get(
            "RecordError WHERE task.id = '{}' ORDER BY last_attempted_at LIMIT {}".format(
            self.task_id, limit))

        self.print_h2("Task {}: {} RecordError(s):", self.task_id, len(errors))
        for error in errors:
            self.retry_record_error(error)

    def retry_all_record_errors(self, limit=100):
        """Retries all errors, 1 page at a time.
        """
        error_count = orientdb.lookup("RecordError", 'task.id', self.task_id, "COUNT(*)")
        self.print_h2("Task {}: {} Total RecordError(s):", self.task_id, error_count)
        while error_count > 0:
            self.retry_record_errors(limit)
            error_count -= limit

    def process_ids(self, ids):
        for rec_id in ids:
            self.process_one(rec_id)

    @staticmethod
    def print_h2(string, *format_args):
        if format_args:
            string = string.format(*format_args)
        print('')
        print(string)
        print('-' * len(string))


class UpdateTask(Task):
    """Checks for updates to one record type
    """
    projections = '*'
    fetchplan = '*:-1 [*]in_*:-2 [*]out_*:-2'
    key_field = 'id'

    def __init__(self, task_id, from_class, where=None):
        """
        Args:
            task_id: The id of the task to save to the database
            from_class: Tuple of the module and type, which together are an OrientDB Class
            where: A where statement to filter the results by
        """
        (self.from_module, self.from_type) = from_class
        self.from_class = '{}{}'.format(self.from_module, self.from_type.title())
        self._lets = []
        self._filters = ([where] if where else [])
        super(UpdateTask, self).__init__(task_id)

    def add_let(self, name, expr):
        """Add a let clause and select it"""
        self._lets.append('{} = {}'.format(name, expr))
        self.projections += ', {}'.format(name)
        self.fetchplan += ' {}:0'.format(name)
        return self

    def add_filter(self, where):
        self._filters.append(where)
        return self

    def _step(self, after, options):
        self.print_h2("Getting {} records updated after {} :".format(
            self.from_class, after))
        where = self._format_where(after=after)
        limit = 100
        count = self._get_count(where)
        for skip in range(0, count, limit):
            query = self._create_query(where=where, skip=skip, limit=limit)
            response = orientdb.execute(query)
            if response:
                self._process_response(response)

    def process_ids(self, ids):
        where = self._format_where(rec_id=ids)
        q = self._create_query(where=where, limit=len(ids))
        response = orientdb.execute(q)
        if not response:
            raise Exception('{} not found: {}'.format(self.from_class, ids))
        return self._process_response(response)

    def process_one(self, rec_id):
        return self.process_ids([rec_id])

    def _get_count(self, where):
        query = ("SELECT count(*) AS c FROM {from_class} {where}").format(
            from_class=self.from_class, where=where)
        return orientdb.execute(query)[0]['c']

    def _create_query(self, where='', skip=0, limit=100):
        # generate query for one page of unfiltered records
        single_page_query = (
            "SELECT FROM {from_class} "
            "{where} "
            "ORDER BY _updated_at ASC "
            "SKIP {skip} "
            "LIMIT {limit} "
        ).format(
            from_class=self.from_class,
            where=where,
            skip=skip,
            limit=limit,
        )
        # filter each page of records (don't paginate filtered records)
        return self._wrap_base_query(single_page_query)

    def _wrap_base_query(self, base_query):
        """Wrap the base query with configuration for this instance"""
        return (
            "SELECT {projections} "
            "FROM ({base_query}) {lets} "
            "{filters} "
            "FETCHPLAN {fetchplan} "
        ).format(
            projections=self.projections,
            base_query=base_query,
            lets=self._format_lets(),
            filters=self._format_filters(),
            fetchplan=self.fetchplan,
        )

    def _format_lets(self):
        """Format LETs like "LET expr, expr"."""
        if not self._lets:
            return ""
        lets_str = ', '.join(self._lets)
        return 'LET {}'.format(lets_str)

    def _format_filters(self):
        """Format filters like "WHERE (condition) AND (condition)"."""
        if not self._filters:
            return ""
        filters = ["({})".format(f) for f in self._filters]
        return "WHERE " + " AND ".join(filters)

    def _format_where(self, after=None, rec_id=None):
        if rec_id:
            rec_id_str = orientdb.encode(rec_id)
            return "WHERE {} IN {}".format(self.key_field, rec_id_str)
        elif after:
            return "WHERE _updated_at > '{}'".format(after)
        return ""

    def _process_response(self, response):
        """Processes one page of records from the database.
        """
        results = {}
        for (i, obj) in enumerate(response):
            with log.ErrorLogging(self.task_id, obj):
                results[i] = self._process_response_record(obj)
        return results

    def _process_response_record(self, obj):
        raise NotImplementedError("Please implement this method in a child class")


class ReflectTask(UpdateTask):
    """Checks for updates to one record type and updates another system.
    """
    link_format = "{from_property}.BOTH('Reflection')[@class='{to_class}']"
    link_field = None

    # if enabled, will prevent multiple Update edges
    upsert_edge = False
    # if enabled, will track previous Updated upsert times in Updated.history
    edge_history = False

    def __init__(self, from_class, to_class, where=None, with_reflection=None):
        """
        Args:
            from_class: Tuple of the module and type, which together are an OrientDB Class
            to_class: Tuple of the module and type, which together are an OrientDB Class
            where: A where statement to filter the results by
            with_reflection: Whether or not to restrict results to those with or without the edge
        """
        (self.to_module, self.to_type) = to_class
        self.to_class = '{}{}'.format(self.to_module, '{}{}'.format(self.to_type[0].title(), self.to_type[1:]))

        task_id = '{}{} > {}'.format(from_class[0], from_class[1].title(), self.to_class)

        super(ReflectTask, self).__init__(task_id, from_class, where=where)

        self.with_connection('@this', self.to_class, '_linked_to',
            filter_results=with_reflection, field=self.link_field)

    def with_connection(self,
            from_property,
            to_class,
            as_property,
            filter_results=None,
            field=None):
        """Adds a reflection to the original query.

        Args:
            to_class: The orientdb class name
            filter_results: True or False to restrict results to those with or without the edge
            as_property: What property to return the reflection "as"
            field: Instead of getting the whole record, get a single property
        """
        link_phrase = self.link_format.format(
            from_property=from_property, to_module=self.to_module, to_class=to_class)

        if filter_results is False:
            # only get objects without a Reflection
            self.add_filter("{}.size() = 0".format(link_phrase))
        else:
            if filter_results:
                # only get objects with a Reflection
                self.add_filter("{}.size() > 0".format(link_phrase))

            # add the Reflection to the projections
            if field:
                self.projections += ", LAST({}.{}) AS {} ".format(link_phrase, field, as_property)
            else:
                self.projections += ", LAST({}) AS {} ".format(link_phrase, as_property)

        return self

    def _save_response(self, from_obj, result_rec, is_update=False, request=None):
        reflection_type = 'Updated' if is_update else 'Created'

        # save the response to orientdb
        with graceful.Uninterruptable():
            result_rec.throw_at_orient()

            # draw the edge
            edge_content = {'request': request} if request else None
            if reflection_type == 'Updated' and self.upsert_edge:
                orientdb.upsert_edge(
                    from_obj['@rid'], result_rec.rid,
                    track_history=self.edge_history, content=edge_content)
            else:
                orientdb.create_edge(
                    from_obj['@rid'], result_rec.rid,
                    classname=reflection_type, content=edge_content)

            print(u'{} {} {}  –{}→  {} {} {}'.format(
                from_obj['@rid'],
                from_obj['@class'],
                from_obj.get('id', ''),
                reflection_type,
                result_rec.rid,
                result_rec.classname,
                result_rec.get(self.link_field or 'id', '')))

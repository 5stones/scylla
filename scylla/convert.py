from datetime import datetime, timedelta, tzinfo
import collections
import re
import inspect
import copy


class Conversion(object):
    """Converts one object or dict to another using a conversion dict.

    Attributes:
        conversion_map: A dict of new_fields to static values and conversions:
            new_fieldname: static_value,
            new_fieldname: ( old_fieldname, ),
            new_fieldname: ( old_fieldname, conversion_function )
        post_convert: Function to call after conversion like f(from_obj, to_obj) return to_obj
    """
    def __init__(self, conversion_map=None, post_convert=None):
        self.conversion_map = conversion_map or {}
        self.post_convert = post_convert

    def add_static_value(self, fieldname, value):
        self.conversion_map[fieldname] = value

    def add_conversion(self, new_fieldname, old_fieldname, conversion_function=None):
        """Adds a conversion from one field to another.

        Args:
            new_fieldname: The name of the field to set on the new object
            old_fieldname: The name of the field to get from the old object
            conversion_function(value,obj): A function that can be applied to the value that takes two parameters:
                value: the value of old_fieldname taken from the original object
                obj: the original object
        """
        if not conversion_function:
            self.conversion_map[new_fieldname] = (old_fieldname, )
        else:
            self.conversion_map[new_fieldname] = (old_fieldname, conversion_function)

    def convert(self, obj, included_fields=None):
        """Converts the object to a new dict.
        """
        if isinstance(self.conversion_map, collections.OrderedDict):
            # the conversion_map is ordered, so the result should be ordered
            o = collections.OrderedDict()
        else:
            o = {}

        # map old fields to new
        for (field, conversion) in self.conversion_map.items():
            if included_fields is None or field in included_fields:
                o[field] = self._convert_field(obj, conversion)

        if self.post_convert:
            o = self.post_convert(obj, o) or o
        return self._post_convert(obj, o) or o

    def _post_convert(self, obj, result):
        return result

    def __call__(self, obj):
        return self.convert(obj)

    @classmethod
    def _convert_field(cls, obj, conversion, parent_obj=None):
        """Gets a single converted value from the original object.

        Args:
            obj: The original object to lookup values from
            conversion: How to get the new value from the original object:
                static_value
                ( old_fieldname|conversion_function(value)|conversion_function(value,obj), ...),
        Returns:
            The converted value
        """
        if not isinstance(conversion, tuple):
            # static value (independent of object being converted)
            # return a deep copy as if static elements are appended to
            # it will incorrectly add data to future conversions
            return copy.deepcopy(conversion)

        # either call the function passed, or pull a value from the dict
        conv = conversion[0]
        if callable(conv):
            num_args = cls.get_num_args(conv)
            if num_args > 1:
                val = conv(obj, parent_obj)
            else:
                val = conv(obj)
        elif isinstance(conv, basestring) and conv[0] == '.':
            val = getattr(obj, conv[1:], None)
        elif isinstance(obj, list):
            try:
                val = obj[conv]
            except IndexError:
                val = None
        else:
            val = obj.get(conv)

        if len(conversion) > 1 and val is not None:
            # recusively continue converting
            return cls._convert_field(val, conversion[1:], parent_obj=obj)

        return val

    @staticmethod
    def get_num_args(func):
        try:
            arg_spec = inspect.getargspec(func)
            num_args = len(arg_spec[0])
            if inspect.ismethod(func):
                num_args -= 1
        except TypeError:
            num_args = 1
        return num_args


class UTC(tzinfo):
    """UTC timezone."""
    td = timedelta(0)

    def utcoffset(self, dt):
        return self.td

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return self.td


class FixedTz(tzinfo):
    """Fixed timezone with a name like "-06:00"."""

    def __init__(self, hours=0, minutes=0):
        if hours is None:
            self.hours = 0
        else:
            self.hours = int(hours)

        if minutes is None:
            self.minutes = 0
        else:
            self.minutes = int(minutes)

        self.td = timedelta(hours=self.hours, minutes=self.minutes)

    def utcoffset(self, dt):
        return self.td

    def tzname(self, dt):
        if self.hours > 0:
            return "+{0}:{1}".format(self.hours, self.minutes)
        else:
            return "{0}:{1}".format(self.hours, self.minutes)

    def dst(self, dt):
        return self.td


# conversions

def parse_iso_date(s):
    """Parses an ISO 8601 datetime string into a datetime object."""
    if isinstance(s, datetime):
        return s
    if not s:
        return None

    r = re.compile(r'^(\d{4})-?(\d{2})?-?(\d{2})?[T ]?(\d{2})?:?(\d{2})?:?(\d{2})?\.?(\d+)?(Z|[+-]\d{2})?:?(\d{2})?')
    try:
        groups = list(r.search(s).groups())
    except AttributeError:
        return None
    # timezone tuple
    tz = groups[7:]

    # convert subseconds to microseconds
    if groups[6] is not None:
        subseconds = float("0.{}".format(groups[6]))
        microseconds = int(round(subseconds * 1000000))
        groups[6] = microseconds
    else:
        groups[6] = 0

    # datetime tuple
    groups = map(int, filter(None, groups[:7]))
    # create tzinfo object
    if tz[0] is None:
        tzinfo_ = None
    elif tz[0] == 'Z':
        tzinfo_ = UTC()
    else:
        tzinfo_ = FixedTz(*tz)

    return datetime(*groups, tzinfo=tzinfo_)


def as_mysql_datetime(d):
    """Parses a datetime string, and converts it to a mysql date or datetime."""
    if not isinstance(d, datetime):
        d = parse_iso_date(d)

    if isinstance(d, datetime):
        if d.tzinfo:
            d = d.astimezone(UTC())
        return d.strftime('%Y-%m-%d %H:%M:%S')
    elif d:
        return d.strftime('%Y-%m-%d')


def search_list(l, keys, value):
    """Searches a list of dicts for the first dict where key=value.
    """
    try:
        for el in l:
            if isinstance(keys, tuple):
                # lookup deeper than 1 level
                el_val = el
                for key in keys:
                    el_val = el_val[key]

                if el_val == value:
                    return el
            elif el[keys] == value:
                return el
    except TypeError:
        return None

    return None


def __main():
    # test iso parsing
    for s in ['20150102121314', '2015-01-02T12:13:14Z', '2015-01-02T12:13:14-06', '2015-01-02T12:13:14-0600', '2015-01-02T12:13:14.123456+06:00']:
        print(s)
        d = parse_iso_date(s)
        print("iso: {0}".format(d))
        print("")

    # test conversion
    data = {
        'field': 'value',
        'subdict': {
            'subfield': 'nested value'
        }
    }
    conv = Conversion({
        'new-static': 'static',
        'new-field': ('field',),
        'new-field-upper': ('field', lambda s: s.upper()),
        'new-subfield': ('subdict', 'subfield'),
        'new-subfield-upper': ('subdict', 'subfield', lambda s: s.upper()),
        'missing-field': ('missing',),
        'missing-subfield': ('missing', 'missing', ),
    })
    print(conv.convert(data))


if __name__ == '__main__':
    __main()

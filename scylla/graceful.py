# -*- coding: utf-8 -*-
"""Handles graceful shutdowns and processes that should not be interrupted

Examples:
GracefulShutdown.register(some_cleanup_function)

with Uninterruptable():
    response = client.send_record()
    save_response(response)
"""
import signal
import logging


class GracefulShutdown(object):
    """Handles graceful shutdown with multiple cleanup functions"""

    # number of seconds after starting to shutdown before logging a warning
    warning_duration = 9

    _logger = logging.getLogger('scylla')
    _exit_code = 0
    _terminating = False
    _stack = set()

    @classmethod
    def register(cls, func):
        """Register a function that needs to be run before shutting down

        If the function call returns True, we'll wait for a call to
        GracefulShutdown.done(func) before shutting down.
        """
        cls._stack.add(func)
        cls._logger.debug('GracefulShutdown added: %s', len(cls._stack))
        if cls._terminating:
            cls._logger.warning('Shutdown function registered while shutting down.')
            cls._call(func)

    @classmethod
    def done(cls, func):
        """Remove a registered function or flag it as completed"""
        cls._stack.remove(func)
        cls._logger.debug('GracefulShutdown removed %s', len(cls._stack))
        if cls._terminating and len(cls._stack) == 0:
            cls._terminate()

    @classmethod
    def init_handlers(cls):
        """Register GracefulShutdown for SIGTERM and SIGINT"""
        signal.signal(signal.SIGTERM, cls.handler)
        signal.signal(signal.SIGINT, cls.handler)
        if not signal.getsignal(signal.SIGALRM) and cls.warning_duration:
            signal.signal(signal.SIGALRM, cls._alarm_handler)

    @classmethod
    def handler(cls, signum, frame):
        """Signal handling function that will start the shutdown"""
        cls._logger.debug('Signal received: %s', signum)
        if signum == signal.SIGTERM:
            cls._exit_code = 143
        else:
            cls._exit_code = 1
        cls.start()

    @classmethod
    def start(cls):
        """Start the graceful shutdown"""
        cls._terminating = True
        cls._start_timer()
        for func in cls._stack:
            cls._call(func)

        if len(cls._stack) == 0:
            cls._terminate()
        else:
            cls._logger.warning('Waiting for %s graceful shutdown processes',
                                len(cls._stack))

    @classmethod
    def is_shutting_down(cls):
        """Returns true if something has asked us to exit"""
        return cls._terminating

    @classmethod
    def _call(cls, func):
        if func() != True:
            # cleanup function did not return true, so we'll assume it's done
            cls.done(func)

    @classmethod
    def _start_timer(cls):
        if signal.getsignal(signal.SIGALRM) == cls._alarm_handler:
            signal.alarm(cls.warning_duration)

    @classmethod
    def _alarm_handler(cls, signum, frame):
        cls._logger.warning('Graceful shutdown taking longer than %s seconds',
                            cls.warning_duration)

    @classmethod
    def _terminate(cls):
        cls._logger.info('Graceful shutdown complete')
        exit(cls._exit_code)


class Uninterruptable(object):
    """A context manager which will prevent exiting while running a process"""
    _logger = logging.getLogger('scylla')

    @property
    def is_shutting_down(self):
        return GracefulShutdown.is_shutting_down()

    def shutdown(self):
        self._logger.debug('Shutdown triggered during Uninterruptable')
        # just continue what we were doing and tell GracefulShutdown to wait
        return True

    def __enter__(self):
        GracefulShutdown.register(self.shutdown)

    def __exit__(self, exc_type, exc_val, exc_tb):
        GracefulShutdown.done(self.shutdown)

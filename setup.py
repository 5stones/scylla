from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla',
    packages = ['scylla', 'scylla.orientdb'],
    version = version,

    description = 'Scylla is an integration hub designed to save schemaless records to an OrientDB server, along with how records are linked between systems.',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla',
    download_url = 'https://gitlab.com/5stones/scylla/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration',

    classifiers=[
        #'Development Status :: 4 - Beta',
        'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        #'License :: OSI Approved :: MIT License',

        #'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'requests[security]>=2.11.1',
        'python-daemon>=1.5',
        'pyorient>=1.5.5',
    ],
)

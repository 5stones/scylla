## [1.5.1](https://gitlab.com/5stones/scylla/compare/v1.5.0...v1.5.1) (2021-08-06)


### Bug Fixes

* **log:** fix configuration import ([f8cce60](https://gitlab.com/5stones/scylla/commit/f8cce60))



# [1.5.0](https://gitlab.com/5stones/scylla/compare/v1.4.1...v1.5.0) (2021-08-06)


### Features

* **log:** add config so that errors will not be saved to orientdb ([bd6fce1](https://gitlab.com/5stones/scylla/commit/bd6fce1))



## [1.4.1](https://gitlab.com/5stones/scylla/compare/v1.4.0...v1.4.1) (2021-01-22)


### Bug Fixes

* **tf/k8s-deployments/main.tf:** fix requests and limits declarations ([61a580e](https://gitlab.com/5stones/scylla/commit/61a580e))



# [1.4.0](https://gitlab.com/5stones/scylla/compare/v1.3.4...v1.4.0) (2020-11-12)


### Features

* **tf:** create module for k8s-deployments ([649594a](https://gitlab.com/5stones/scylla/commit/649594a))



## [1.3.4](https://gitlab.com/5stones/scylla/compare/v1.3.3...v1.3.4) (2020-09-17)



## [1.3.3](https://gitlab.com/5stones/scylla/compare/v1.3.2...v1.3.3) (2020-07-23)


### Bug Fixes

* **reflection_cleaner:** fix error handling and retain_count ([1f4d5fc](https://gitlab.com/5stones/scylla/commit/1f4d5fc))



## [1.3.2](https://gitlab.com/5stones/scylla/compare/v1.3.1...v1.3.2) (2020-07-21)


### Bug Fixes

* **orientdb:** fix edge upsert ([5b2b36e](https://gitlab.com/5stones/scylla/commit/5b2b36e))



## [1.3.1](https://gitlab.com/5stones/scylla/compare/v1.3.0...v1.3.1) (2020-07-21)



# [1.3.0](https://gitlab.com/5stones/scylla/compare/v1.2.0...v1.3.0) (2020-07-21)


### Features

* **orientdb:** add utility to clean up Updated edges ([e71b9bd](https://gitlab.com/5stones/scylla/commit/e71b9bd))



# [1.2.0](https://gitlab.com/5stones/scylla/compare/v1.1.1...v1.2.0) (2020-06-24)


### Features

* **orientdb:** add support for binary through pyorient ([45a73d9](https://gitlab.com/5stones/scylla/commit/45a73d9))
* **orientdb:** function for a batch of commands in one request ([4d431a9](https://gitlab.com/5stones/scylla/commit/4d431a9))
* **tasks:** allow upserting of Updated edges for ReflectTasks ([db69ca8](https://gitlab.com/5stones/scylla/commit/db69ca8))



## [1.1.1](https://gitlab.com/5stones/scylla/compare/v1.1.0...v1.1.1) (2019-11-07)


### Bug Fixes

* **log:** fix logging exceptions with unicode characters ([a9d6807](https://gitlab.com/5stones/scylla/commit/a9d6807))
* **log:** improve logging to stdout ([8cff6da](https://gitlab.com/5stones/scylla/commit/8cff6da))
* **tf:** upgrade for terraform v0.12.x ([3ca875a](https://gitlab.com/5stones/scylla/commit/3ca875a))


### Features

* **tf:** add terraform files for deploying to AWS ECS ([fc979e0](https://gitlab.com/5stones/scylla/commit/fc979e0))


### Performance Improvements

* **orientdb:** make slow query log configurable and log to one line ([2d69e82](https://gitlab.com/5stones/scylla/commit/2d69e82))



## [1.0.1](https://gitlab.com/5stones/scylla/compare/v1.0.0...v1.0.1) (2019-01-21)


### Features

* **tasks:** save Task record on start ([5929928](https://gitlab.com/5stones/scylla/commit/5929928)), closes [#2](https://gitlab.com/5stones/scylla/issues/2)



# [1.0.0](https://gitlab.com/5stones/scylla/compare/7ce8086...v1.0.0) (2018-12-04)


### Bug Fixes

* **app:** fix time period for stats ([da90b51](https://gitlab.com/5stones/scylla/commit/da90b51))
* **Conversion:** Fix an issue where statically defined elements can be appended to and carry data fo ([98c726e](https://gitlab.com/5stones/scylla/commit/98c726e))
* **Conversion:** fix hidden method warnings ([c6f0c43](https://gitlab.com/5stones/scylla/commit/c6f0c43))
* **convert:** fix error when unable to parse a date ([cfff9ce](https://gitlab.com/5stones/scylla/commit/cfff9ce))
* **convert:** fix parsing iso dates if it's already a datetime ([0586099](https://gitlab.com/5stones/scylla/commit/0586099))
* **convert:** fix referencing indexes that are out of range ([802a11c](https://gitlab.com/5stones/scylla/commit/802a11c))
* **ipre, elan:** get order import working ([a3e6cb7](https://gitlab.com/5stones/scylla/commit/a3e6cb7))
* **log:** fix handling of unicode keys ([4462974](https://gitlab.com/5stones/scylla/commit/4462974))
* **log:** fix typo ([fec75fe](https://gitlab.com/5stones/scylla/commit/fec75fe))
* **main:** check for args length before stats call ([d13163a](https://gitlab.com/5stones/scylla/commit/d13163a))
* **orientdb:** fix issue with silent failures on upsert ([90768fd](https://gitlab.com/5stones/scylla/commit/90768fd))
* **orientdb:** fix or syntax ([d710204](https://gitlab.com/5stones/scylla/commit/d710204))
* **orientdb:** ignore warnings when using a self signed cert ([3b26014](https://gitlab.com/5stones/scylla/commit/3b26014))
* **orientdb, log:** fix handling of unicode ([b9b029b](https://gitlab.com/5stones/scylla/commit/b9b029b))
* **parse_iso_date:** Fix issue with parsing of dates with nanoseconds ([2e9f463](https://gitlab.com/5stones/scylla/commit/2e9f463))
* **records:** fix finding subclasses of subclasses ([c53d56a](https://gitlab.com/5stones/scylla/commit/c53d56a))
* **records:** fix parsing of Decimal fields ([9a1ed09](https://gitlab.com/5stones/scylla/commit/9a1ed09))
* **records:** fix parsing of decimal when it's numeric ([62771ba](https://gitlab.com/5stones/scylla/commit/62771ba))
* **records:** fix processing of null or '' values for ints and decimals ([4b26b38](https://gitlab.com/5stones/scylla/commit/4b26b38))
* **records:** fix saving of linked records ([f8ed6ea](https://gitlab.com/5stones/scylla/commit/f8ed6ea))
* **ReflectTask:** Fix issue with casing in to_class name ([17ff5f7](https://gitlab.com/5stones/scylla/commit/17ff5f7))
* **rest:** fix debug command ([7ce8086](https://gitlab.com/5stones/scylla/commit/7ce8086))
* **stats:** fix task summary query ([625e1e2](https://gitlab.com/5stones/scylla/commit/625e1e2))
* **tasks:** fix bug where last_run would be cleared on an exception ([586d479](https://gitlab.com/5stones/scylla/commit/586d479))
* **tasks:** fix creating an edge without request data ([5414684](https://gitlab.com/5stones/scylla/commit/5414684))
* **tasks:** fix pagination, which was skipping records ([363abc5](https://gitlab.com/5stones/scylla/commit/363abc5))
* **UpdateTask:** Fix issue with LET context by moving projections down a level ([794aaed](https://gitlab.com/5stones/scylla/commit/794aaed))
* fix setting of RecordError.created_at so it's not overridden when reattempted ([402525c](https://gitlab.com/5stones/scylla/commit/402525c))
* **tasks:** fix strict sql error ([f1676b3](https://gitlab.com/5stones/scylla/commit/f1676b3))


### Features

* **app:** add a simple way to get processing statistics ([d6756b1](https://gitlab.com/5stones/scylla/commit/d6756b1))
* **app:** add argument to stats to get a specific metric ([ce48c91](https://gitlab.com/5stones/scylla/commit/ce48c91))
* **App:** allow running in the foreground ([baee5e3](https://gitlab.com/5stones/scylla/commit/baee5e3))
* **app, stats:** add command to summarize activity of tasks and edges ([8513637](https://gitlab.com/5stones/scylla/commit/8513637))
* **Conversion:** support collections.OrderedDict for ordered results ([2e088fb](https://gitlab.com/5stones/scylla/commit/2e088fb))
* **graceful:** add GracefulShutdown ([c2de0e8](https://gitlab.com/5stones/scylla/commit/c2de0e8))
* **health:** add ability to check app health ([1c55a02](https://gitlab.com/5stones/scylla/commit/1c55a02))
* **log:** add function to log to stderr ([7e5e8bc](https://gitlab.com/5stones/scylla/commit/7e5e8bc))
* **log:** save details attribute from exceptions ([e3d8754](https://gitlab.com/5stones/scylla/commit/e3d8754))
* **orientdb:** add option for IS (NOT) NULL to __format_where ([64f54a0](https://gitlab.com/5stones/scylla/commit/64f54a0))
* **records:** add function to get the subclass as the factory does ([b1200b2](https://gitlab.com/5stones/scylla/commit/b1200b2))
* **records:** generalize parsing of objects without types ([12d79d3](https://gitlab.com/5stones/scylla/commit/12d79d3))
* **setup.py, package.json, .gitignore:** Add versioning scripts and utilities ([8fbbbbb](https://gitlab.com/5stones/scylla/commit/8fbbbbb))
* **stats:** add overall status message to summary command ([720dbe5](https://gitlab.com/5stones/scylla/commit/720dbe5))
* **stats:** improve readability for variable width email ([32bae7b](https://gitlab.com/5stones/scylla/commit/32bae7b))
* **Task:** Add a setter to the Task class which rebuilds the TaskContext ([cff1c6c](https://gitlab.com/5stones/scylla/commit/cff1c6c))
* **tasks:** add ability to use LETs in UpdateTask ([fd27eb0](https://gitlab.com/5stones/scylla/commit/fd27eb0))




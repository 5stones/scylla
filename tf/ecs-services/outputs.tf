output "task_definitions" {
  value = aws_ecs_task_definition.main
}

output "services" {
  value = aws_ecs_service.main
}

variable "cluster" {
  description = "The name of the ecs cluster to add services to"
}

variable "image" {
}

variable "region" {
  default = "us-east-1"
}

variable "apps" {
  description = "A list of apps to launch run from the docker image (without .py)"
  default     = []
}

variable "task_prefix" {
  default = "scylla-production"
}

variable "service_prefix" {
  default = "scylla"
}

variable "cpu" {
  default = 128
}

variable "memoryReservation" {
  default = 64
}

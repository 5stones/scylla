resource "aws_ecs_task_definition" "main" {
  for_each = toset(var.apps)

  family = "${var.task_prefix}-${each.key}"

  requires_compatibilities = ["EC2"]
  volume {
    name      = "config"
    host_path = "/mnt/efs/scylla/config.py"
  }

  container_definitions = <<EOF
[
  {
    "name": "scylla",
    "image": "${var.image}",
    "cpu": ${var.cpu},
    "memoryReservation": ${var.memoryReservation},
    "essential": true,
    "mountPoints": [
      {
        "sourceVolume": "config",
        "containerPath": "/usr/src/app/config.py"
      }
    ],
    "command": [
      "${each.key}.py"
    ],
    "healthCheck": {
      "command": [
        "python", "-m", "scylla.status", "1800"
      ],
      "interval": 300,
      "timeout": 10,
      "retries": 1,
      "startPeriod": 300
    },
    "environment": [
      {"name": "AWS_DEFAULT_REGION", "value": "${var.region}"}
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.task_prefix}",
        "awslogs-region": "${var.region}",
        "awslogs-stream-prefix": "${each.key}"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "main" {
  for_each = toset(var.apps)

  name = "${var.service_prefix}-${each.key}"
  cluster = data.aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.main[each.key].arn

  launch_type = "EC2"
  desired_count = 1

  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent = 100

  ordered_placement_strategy {
    type = "spread"
    field = "instanceId"
  }
}

data "aws_ecs_cluster" "main" {
  cluster_name = var.cluster
}

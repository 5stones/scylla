resource "kubernetes_deployment" "main" {
  for_each = toset(var.apps)

  metadata {
    namespace = var.namespace
    name      = "${var.name_prefix}${each.key}"
    labels    = merge(var.labels, { app = "${var.name_prefix}${each.key}" })
  }

  spec {
    replicas = 1

    selector {
      match_labels = merge(var.labels, { app = "${var.name_prefix}${each.key}" })
    }

    strategy {
      type = "Recreate"
    }

    template { # pod template
      metadata {
        labels = merge(var.labels, { app = "${var.name_prefix}${each.key}" })
        annotations = var.pod_annotations
      }

      spec {
        service_account_name = kubernetes_service_account.main.metadata.0.name

        container {
          name  = "scylla"
          image = var.image

          args = ["${each.key}.py"]

          resources {
            requests = {
              cpu    = var.resources.requests.cpu
              memory = var.resources.requests.memory
            }
            limits = {
              cpu    = var.resources.limits.cpu
              memory = var.resources.limits.memory
            }
          }

          dynamic "env" {
            for_each = var.env
            content {
              name  = env.key
              value = env.value
            }
          }

          volume_mount {
            name       = "config"
            mount_path = "/usr/src/app/config"
            read_only  = true
          }

          liveness_probe {
            exec {
              command = ["python", "-m", "scylla.status", "1800"]
            }
            initial_delay_seconds = 300
            period_seconds        = 300
            timeout_seconds       = 10
            failure_threshold     = 1
          }
        }

        volume {
          # since k8s can only mount secrets as folder, map the config file inside a module folder
          name = "config"
          secret {
            secret_name = var.secret_name
            items {
              key  = "config.py"
              path = "__init__.py"
            }
          }
        }

        dns_policy = var.dns_policy
        dns_config {
          searches = var.search_domains
        }
      }
    }
  }
}

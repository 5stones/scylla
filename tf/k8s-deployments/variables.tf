variable "namespace" {
  default = "default"
}

variable "name_prefix" {
  default = "scylla-"
}

variable "image" {
}

variable "apps" {
  description = "A list of apps to launch run from the docker image (without .py)"
  default     = []
}

variable "labels" {
  default = {}
}

variable "secret_name" {
  default = "scylla"
}

variable "env" {
  default = []
}

variable "resources" {
  default = {
    requests = {
      cpu    = "128m"
      memory = "64Mi"
    }
    limits = {
      cpu    = null
      memory = null
    }
  }
}

variable "dns_policy" {
  default = "ClusterFirst"
}

variable "search_domains" {
  default = []
}

variable "pod_annotations" {
  description = "annotations for pods such as `fluentbit.io/parser = scylla`"
  default     = {}
}

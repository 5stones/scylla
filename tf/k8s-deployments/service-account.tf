resource "kubernetes_service_account" "main" {
  metadata {
    namespace = var.namespace
    name      = var.secret_name
  }
}
